<?php
/*
Plugin Name: SSN Botnation
Description: Create easily your own AI chatbot for Word Press powered by Botnation AI. Free chatbot building platform.
Version: 1.0.0
Text Domain: ssn-botnation
Author: Kushuh
Domain Path: /languages
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require_once plugin_dir_path(__FILE__) . 'admin/configuration.php';

class SsnBotnation
{
    public function activate() {
        flush_rewrite_rules();

        global $wpdb;

        $table_name = $wpdb->prefix . "ssn_botnation_lang";
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id VARCHAR(10) NOT NULL,
            api TEXT NOT NULL,
            websiteID TEXT NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    public function deactivate()
    {
        flush_rewrite_rules();
    }

    public static function uninstall()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . "ssn_botnation_lang";
        $wpdb->query( "DROP TABLE IF EXISTS $table_name" );
    }

    public function register_styles()
    {

    }

    public function register_admin_scripts()
    {
        wp_register_script(
            'ssn-bn-backend',
            plugin_dir_url( __FILE__ ) . 'admin/scripts/save_key.js',
            array(),
            '1.0.4',
            false
        );

        wp_enqueue_script( 'ssn-bn-backend' );

        wp_localize_script( 'ssn-bn-backend', 'wpSave',
            array(
                'url' => content_url( '/plugins/ssn-botnation/admin/save_key.php' )
            )
        );
    }

    public function register_frontend_scripts()
    {
        $my_current_lang = apply_filters( 'wpml_current_language', NULL );

        global $wpdb;
        $table_name = $wpdb->prefix . "ssn_botnation_lang";
        $sql = "SELECT * FROM $table_name WHERE id = %s";
        $key = $wpdb->get_row( $wpdb->prepare( $sql,  $my_current_lang ) );

        wp_register_script(
            'ssn-url',
            plugin_dir_url( __FILE__ ) . 'scripts/url.js',
            array(),
            '1.0.1',
            false
        );

        wp_enqueue_script( 'ssn-url' );

        wp_register_script(
            'ssn-bg-load',
            plugin_dir_url( __FILE__ ) . 'scripts/bg_load.js',
            array(),
            '1.0.69',
            false
        );

        wp_enqueue_script( 'ssn-bg-load' );

        wp_register_script(
            'ssn-cb-ev',
            plugin_dir_url( __FILE__ ) . 'scripts/cb_ev.js',
            array(),
            '1.0.13',
            false
        );

        wp_enqueue_script( 'ssn-cb-ev' );

        wp_register_script(
            'ssn-cb-start',
            plugin_dir_url( __FILE__ ) . 'scripts/cb_start.js',
            array(),
            '1.0.9',
            false
        );

        wp_enqueue_script( 'ssn-cb-start' );

        wp_localize_script( 'ssn-cb-start', 'wpScript',
            array(
                'key' => $key
            )
        );
    }

    public function register_scripts()
    {
        add_action( 'admin_enqueue_scripts', array($this, 'register_admin_scripts') );
        add_action( 'wp_enqueue_scripts', array($this, 'register_frontend_scripts') );
    }

    public function register()
    {
        $this->register_scripts();
        $this->register_styles();

        // activation.
        register_activation_hook( __FILE__, array( $this, 'activate' ) );
        // deactivation.
        register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
        // uninstall.
        register_uninstall_hook( __FILE__, array( 'SsnBotnation', 'uninstall' ) );
    }
}

$chatbot = new SsnBotnation();
$chatbot->register();