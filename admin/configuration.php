<?php

add_action( 'admin_menu', 'add_ssn_bn_menu' );

function add_ssn_bn_menu()
{
    $myicon = base64_encode(
        '<svg height="16px" width="16px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <g>
            <path d="M432,0H80C35.817,0,0,35.817,0,80v256c0,44.183,35.817,80,80,80h313.44l91.2,91.36c3.02,2.995,7.107,4.665,11.36,4.64
                c8.837,0,16-7.163,16-16V80C512,35.817,476.183,0,432,0z M480,457.44l-68.64-68.8c-3.019-2.995-7.107-4.665-11.36-4.64H80
                c-26.51,0-48-21.49-48-48V80c0-26.51,21.49-48,48-48h352c26.51,0,48,21.49,48,48V457.44z"/>
            <circle cx="256" cy="208" r="32"/>
            <circle cx="368" cy="208" r="32"/>
            <circle cx="144" cy="208" r="32"/>
        </g>
    </svg>'
    );

    add_menu_page(
        'Botnation Configuration (SSN)',
        'Botnation (SSN)',
        'manage_options',
        'ssn_botnation',
        'admin_page_content',
        'data:image/svg+xml;base64,' . $myicon
    );
}

function get_active_languages()
{
    $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );

    $lang = array();

    if ( !empty( $languages ) )
    {
        foreach( $languages as $l )
        {
            array_push($lang, array('short' => $l['language_code'], 'long' => $l['native_name']));
        }
    }

    return $lang;
}

function admin_page_content()
{
    ?>
    <style>
        .check svg > * {
            stroke-dasharray: 1000;
            stroke-dashoffset: 0;
        }

        .check svg polyline {
            stroke-dashoffset: -100;
            -webkit-animation: dash-check .9s .35s ease-in-out forwards;
            animation: dash-check .9s .35s ease-in-out forwards;
        }

        @keyframes dash-check {
            0% {
                stroke-dashoffset: -100;
            }
            100% {
                stroke-dashoffset: 900;
            }
        }

        .failed svg > * {
            stroke-dasharray: 1000;
            stroke-dashoffset: 0;
        }

        .failed svg line {
            stroke-dashoffset: 1000;
            -webkit-animation: dash .9s .35s ease-in-out forwards;
            animation: dash .9s .35s ease-in-out forwards;
        }

        @keyframes dash {
            0% {
                stroke-dashoffset: 1000;
            }
            100% {
                stroke-dashoffset: 0;
            }
        }
    </style>
    <style>
        .chatbot {
            margin: 0 1rem 0 0;
            display: flex;
            flex-direction: column;
            border-style: solid;
            border-width: thin;
            border-color: #23282d;
        }

        .chatbot_head {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            padding: 0.6rem;
            background-color: #23282d;
            font-size: 1.2rem;
        }

        .chatbot_head_title, .chatbot_head_icons {
            display: flex;
            flex-direction: row;
        }

        .chatbot_head_main_title {
            font-weight: bold;
            color: #fff;
            margin: 0 0.5rem 0 0;
        }

        .chatbot_head_sub_title {
            color: #FFE87F;
        }

        .chatbot_head_icons > * {
            height: 1rem;
            width: 1rem;
            margin: auto 0.1rem;
        }

        .status_icon:not(.loading) .loader,
        .status_icon:not(.status_failed) .failed,
        .status_icon.status_failed .check,
        .status_icon.loading .check {
            display: none!important;
        }

        .loader, .check, .failed {
            height: 1rem;
            width: 1rem;
        }

        .chatbot_content {
            display: flex;
            flex-direction: column;
            padding: 0.6rem;
        }

        .chatbot_content label {
            font-weight: bold;
        }

        .chatbot_content label:not(:first-child) {
            margin: 0.6rem 0 0.3rem 0;
        }

        .chatbot_content label:first-child {
            margin: 0 0 0.3rem 0;
        }
    </style>

    <h1>Chatbots</h1>
    <p><i>Add you chatbots data here.</i></p>
    <hr/>
    <?php

    $languages = get_active_languages();

    if ( !empty( $languages ) )
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "ssn_botnation_lang";
        $sql = "SELECT * FROM $table_name";
        $keys = $wpdb->get_results( $wpdb->prepare( $sql ), 'OBJECT_K' );

        foreach( $languages as $l )
        {
            $api = '';
            $website = '';

            if ( !empty( $keys[ $l['short'] ] ) )
            {
                $data = $keys[ $l['short'] ];
                $api = $data->api;
                $website = $data->websiteID;
            }

            add_chatbot( $l, $api, $website );
        }
    }
}

function add_chatbot($lang, $api = '', $website = '')
{
    ?>
    <div id="<?php echo $lang['short']?>" class="chatbot">
        <div class="chatbot_head">
            <div class="chatbot_head_title">
                <div class="chatbot_head_main_title">
                    <?php echo $lang['long']?>
                </div>
                <div class="chatbot_head_sub_title">
                    <?php echo $lang['short']?>
                </div>
            </div>
            <div class="chatbot_head_icons">
                <div class="status_icon">
                    <img class="loader" src="<?php echo plugins_url( 'ssn-botnation/assets/loader.svg' )?>"/>
                    <div class="check">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="20 20 90 90">
                            <polyline fill="none" stroke="limegreen" stroke-width="6" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                        </svg>
                    </div>
                    <div class="failed">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="20 20 90 90">
                            <line class="path line" fill="none" stroke="#ff3232" stroke-width="6" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                            <line class="path line" fill="none" stroke="#ff3232" stroke-width="6" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="chatbot_content">
            <label>Api key</label>
            <input
                    name="api"
                    value="<?php echo $api?>"
                    placeholder="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
                    onchange="saveKey('<?php echo $lang['short']?>')"
            />

            <label>Website key</label>
            <input
                    name="website"
                    value="<?php echo $website?>"
                    placeholder="xxxxxxxxxxxx"
                    onchange="saveKey('<?php echo $lang['short']?>')"
            />
        </div>
    </div>
    <?php
}