<?php

define( 'SHORTINIT', true );
$path = preg_replace('/wp-content(?!.*wp-content).*/','',__DIR__);
include($path.'wp-load.php');

$POST = json_decode(file_get_contents('php://input'), true);
$response = array();

if ( !isset( $POST['id'] ) )
{
    $response['status'] = "no id provided";
    echo json_encode($response);
}
else
{
    global $wpdb;
    $table_name = $wpdb->prefix . "ssn_botnation_lang";

    $sql = "INSERT INTO $table_name " .
        "(id, api, websiteID) VALUES(%s, %s, %s) " .
        "ON DUPLICATE KEY UPDATE api = %s, websiteID = %s";

    $wpdb->query(
        $wpdb->prepare($sql, $POST['id'], $POST['api'], $POST['website'], $POST['api'], $POST['website'])
    );

    $response['status'] = 'ok';
    echo json_encode($response);
}