wpSave = wpSave || {url: ''};

const statuses = {
    ok: 'status_ok',
    failed: 'status_failed',
    loading: 'loading'
};

const setStatus = (el, status) => {
    const statusHolder = el.querySelector('.status_icon');

    for (const s of Object.keys(statuses)) {
        if (status === s) {
            statusHolder.classList.add(statuses[s]);
        } else {
            statusHolder.classList.remove(statuses[s]);
        }
    }
};

const saveKey = async id => {
    const el = document.querySelector(`#${id}`);

    setStatus(el, 'loading');

    try {
        const data = await fetch(
            wpSave.url,
            {
                method: 'put',
                mode: 'same-origin',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id,
                    api: el.querySelector('[name="api"]').value,
                    website: el.querySelector('[name="website"]').value
                })
            }
        );

        const td = await data.text();
        try {
            const jd = JSON.parse(td);

            if (jd.status !== 'ok') {
                console.error(jd.status);
                setStatus(el, 'failed');
            } else {
                setStatus(el, 'ok');
            }
        } catch {
            console.error(td);
            setStatus(el, 'failed');
        }
    } catch(error) {
        console.error(error);
        setStatus(el, 'failed');
    }
};