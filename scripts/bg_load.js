const loadPage = url => new Promise(
    async resolve => {
        if (url === window.location.href) {
                resolve();
                return;
        }

        window.location.href = url;
        resolve();
    }
);