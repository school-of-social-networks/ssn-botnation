const bnEvtHandler = (eventName, eventArgs) => {
    if (eventName == null) {
        return;
    }

    if (eventArgs == null) {
        return;
    }

    if (eventName === 'persona.avatarClick') {
        switch (wpScript.key.id) {
            case 'fr':
                loadPage('/fr/le-coin-des-enfants/#monsters').catch(console.error);
                break;
            default:
                loadPage('/kids-corner/#monsters').catch(console.error);
                break;
        }

        const header = document.querySelector('#show-hide-header');
        const headerHeight = header == null ? 0 : header.getBoundingClientRect().height;
        window.scrollBy(0, 0 - headerHeight - 20)
    }

    if (Object.hasOwnProperty.call(eventArgs, 'url')) {
        loadPage('/' + eventArgs.url).catch(console.error);
    }
};