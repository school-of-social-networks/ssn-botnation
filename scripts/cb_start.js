wpScript = wpScript || {key: {api: '', websiteID: ''}};
let botnationWidget;

const setBnCtx = (reset = false) => {
    const bnCtx = localStorage.getItem('ssn-bn-ctx');

    if (bnCtx == null || reset) {
        const id = 'bn-ctx#' + new Date().getTime();
        localStorage.setItem('ssn-bn-ctx', id);
        return id;
    } else {
        return bnCtx;
    }
};

const resetCtx = () => {
    const cb_id = setBnCtx(true);
    updateUrl({mode: 'replace', params: {'bgload': '1'}, title: document.title});
    window.location.reload();
    initChatbot(cb_id);
};

const initChatbot = cb_id => {
    console.log('starting chatbot');
    cb_id = cb_id || setBnCtx(false);

    window.chatboxSettings = {
        appKey: wpScript.key.api,
        websiteId: wpScript.key.websiteID,
        autoStart: false,
        userId: cb_id,
        onEvents: bnEvtHandler
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://cbassets.botnation.ai/js/widget.js';
        js.async = true;
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'chatbox-jssdk'));

    window.document.addEventListener('botnation.widget.ready', () => {
        botnationWidget = window.BNAI;
    });
}

if (!window.location.search.includes('bgload=1')) {
    initChatbot();
}

document.addEventListener(
    'ssn_cb_reset',
    resetCtx
);