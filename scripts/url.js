const updateUrl = ({mode, pathname, params, title}) => {
    const url = pathname || window.location.pathname;
    let search = window.location.search;

    if (params != null && params.length) {
        if (search.length === 0) {
            search = '?';
        }

        for (const [k, v] of Object.entries(params)) {
            search += `${k}=${v}&`;
        }

        search = search.slice(0, -1);
    }

    const full =url + search;

    switch (mode) {
        case 'replace':
            window.history.replaceState(null, title, full);
            break;
        default:
            window.history.pushState(null, title, full);
            break;
    }
};